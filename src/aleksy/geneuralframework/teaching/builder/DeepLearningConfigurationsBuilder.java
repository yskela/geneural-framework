package aleksy.geneuralframework.teaching.builder;

import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;

/**
 * Deep learning configurations builder
 */
public class DeepLearningConfigurationsBuilder {
    private DeepLearningConfigurations dlc;

    /**
     * Constructor
     */
    public DeepLearningConfigurationsBuilder() {
        dlc = new DeepLearningConfigurations();
    }

    /**
     * Setter for learning rate
     * @param learningRate to set
     * @return builder
     */
    public DeepLearningConfigurationsBuilder setLearningRate(Double learningRate) {
        dlc.learningRate = learningRate;
        return this;
    }

    /**
     * Setter for iterations number
     * @param iterations to set
     * @return builder
     */
    public DeepLearningConfigurationsBuilder setIterations(int iterations) {
        dlc.iterations = iterations;
        return this;
    }

    /**
     * Creation method
     * @return new instance of {@link DeepLearningConfigurations}
     */
    public DeepLearningConfigurations create() {
        return dlc;
    }
}
