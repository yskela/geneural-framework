package aleksy.geneuralframework.teaching.exception;

/**
 * Genetic algorithm configurations exception
 */
public class GeneticAlgorithmConfigurationsException extends GeneticAlgorithmException {
    public GeneticAlgorithmConfigurationsException() {
        super("Genetic algorithm configurations error.");
    }

    public GeneticAlgorithmConfigurationsException(String message) {
        super("Genetic algorithm configurations error. " + message);
    }
}
