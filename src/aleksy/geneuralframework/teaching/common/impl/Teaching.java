package aleksy.geneuralframework.teaching.common.impl;

import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;
import aleksy.geneuralframework.teaching.logic.GeneticAlgorithm;
import aleksy.geneuralframework.teaching.common.api.TeachingFacadeInterface;
import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmException;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import aleksy.geneuralframework.teaching.logic.DeepLearning;

import java.util.List;

/**
 * Implementation of {@link TeachingFacadeInterface}
 */
public class Teaching implements TeachingFacadeInterface {
    @Override
    public List<NeuralNetworkInterface> geneticAlgorithm(TeachingData teachingData, NeuralNetworkInterface network, GeneticAlgorithmConfigurations geneticAlgorithmConfigurations)
    throws GeneticAlgorithmException {
        return new GeneticAlgorithm().start(teachingData, network, geneticAlgorithmConfigurations);
    }

    @Override
    public NeuralNetworkInterface deepLearning(TeachingData teachingData, NeuralNetworkInterface network,
                                               DeepLearningConfigurations deepLearningConfigurations)
            throws NeuralNetworkException {
        return new DeepLearning().start(teachingData, network, deepLearningConfigurations);
    }
}
