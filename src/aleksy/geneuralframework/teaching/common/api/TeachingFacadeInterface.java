package aleksy.geneuralframework.teaching.common.api;

import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;
import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmException;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teachingdata.model.TeachingData;

import java.util.List;

/**
 * Teaching facade interface
 */
public interface TeachingFacadeInterface {

    /**
     * Start method for genetic algorithm. This implementation use an average value of euclidean distances between
     * output vectors and expected output vectors as rate of neural network. Genetic algorithm calculates all output
     * vectors from {@link TeachingData} for neural network and compares each vector to expected output vector, next
     * it calculates an euclidean distances between this vectors and sets the average value of this distances as rate.
     * Algorithm stops when defined amount of generations will be generated.
     * @param teachingData with packages to analyze distances and create rate value for network
     * @param network basic neural network. This network has no importance in genetic algorithm workflow. On
     *                      this instance method copy() is called, and weights of new network are randomized.
     * @param geneticAlgorithmConfigurations with genetic algorithm parameters
     * @return list of neural networks after evolving process
     * @throws GeneticAlgorithmException when there is problem with genetic algorithm configurations
     */
    List<NeuralNetworkInterface> geneticAlgorithm(TeachingData teachingData,
        NeuralNetworkInterface network, GeneticAlgorithmConfigurations geneticAlgorithmConfigurations)
            throws GeneticAlgorithmException;

    /**
     * Starting method for deep learning algorithm. This algorithm can teach multilayer FEEDFORWARD network.
     * Another networks are not supported yet.
     * @param teachingData data with packages to teach network
     * @param network to teach
     * @param deepLearningConfigurations configurations of deep learning parameters
     * @return neural network after teaching process
     * @throws NeuralNetworkException when some problem with internal structure of neural network exists (e. g.
     *                                when input vector size is not correct)
     */
    NeuralNetworkInterface deepLearning(TeachingData teachingData, NeuralNetworkInterface network,
                                        DeepLearningConfigurations deepLearningConfigurations)
                throws NeuralNetworkException;
}
