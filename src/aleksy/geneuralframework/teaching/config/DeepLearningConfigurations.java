package aleksy.geneuralframework.teaching.config;

/**
 * Deep learning configurations class
 */
public class DeepLearningConfigurations {
    /**
     * Learning rate defines rapidity of learning of neural network. Basically this value is 0.1
     */
    public Double learningRate;
    /**
     * Amount of iterations of deep learning
     */
    public int iterations;
}
