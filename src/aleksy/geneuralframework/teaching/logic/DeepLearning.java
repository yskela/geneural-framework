package aleksy.geneuralframework.teaching.logic;

import aleksy.geneuralframework.common.constants.GeneuralConstants;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.common.util.GeneuralLogger;
import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import aleksy.geneuralframework.teachingdata.model.TeachingDataPackage;

import java.util.List;
import java.util.Random;

/**
 * Deep learning class
 */
public class DeepLearning implements Loggable {

    /**
     * Starting method for deep learning algorithm.
     * @param teachingData data with packages to teach network
     * @param network to teach
     * @param deepLearningConfigurations configurations of deep learning parameters
     * @return neural network after teaching process
     * @throws NeuralNetworkException when some problem with internal structure of neural network exists (e. g.
     *                                when input vector size is not correct)
     */
    public NeuralNetworkInterface start(TeachingData teachingData, NeuralNetworkInterface network,
                                        DeepLearningConfigurations deepLearningConfigurations)
            throws NeuralNetworkException {
        GeneuralLogger.log("Deep learning started...", getSenderName());
        Random r = new Random();
        for(int iteration = 0; iteration < deepLearningConfigurations.iterations; iteration++) {
            if(iteration % 1000 == 0)
                GeneuralLogger.log("Itearion " + iteration + "/" + deepLearningConfigurations.iterations, getSenderName());
            int packageNumber = r.nextInt(teachingData.getDataPackages().size());
            TeachingDataPackage tp = teachingData.getDataPackages().get(packageNumber);
            List<Double> expectedOuts = tp.expectedOuts;
            List<Double> outs = network.f(tp.ins);

            NeuralLayerInterface outputLayer = network.getOutputLayer();

            for(int i = 0; i < outs.size(); i++) {
                outputLayer.getNeurons().get(i).setDefect(expectedOuts.get(i) - outs.get(i));
            }

            NeuralLayerInterface nextLayer = outputLayer;
            for(int i = network.getLayers().size() - 2; i >= 0; i--) {
                NeuralLayerInterface hiddenLayer = network.getLayers().get(i);
                for(int j = 0; j < hiddenLayer.getNeurons().size(); j++) {
                    NeuronInterface neuron = hiddenLayer.getNeurons().get(j);
                    Double defect = 0.0;
                    for(NeuronInterface nextNeuron : nextLayer.getNeurons()) {
                        if(network.getWeightFunctionRecipe() == WeightFunctionRecipe.DEFAULT_LINEAR)
                            defect += nextNeuron.getDefect() * nextNeuron.getWeights().get(j).getFactors().get(0);
                    }
                    neuron.setDefect(defect);
                }
                nextLayer = hiddenLayer;
            }

            NeuralLayerInterface inputLayer = network.getInputLayer();
            for(int i = 0; i < inputLayer.size(); i++) {
                NeuronInterface inputNeuron = inputLayer.getNeurons().get(i);
                for(int j = 0; j < inputNeuron.getWeights().size(); j++) {
                    WeightFunction weight = inputNeuron.getWeights().get(j);
                    if(inputNeuron.getWeightFunctionRecipe() == WeightFunctionRecipe.DEFAULT_LINEAR) {
                        Double factor = weight.getFactors().get(0);
                        Double internalInput = j == tp.ins.size() ? GeneuralConstants.BIAS : tp.ins.get(j);
                        factor = factor + deepLearningConfigurations.learningRate
                                * internalInput
                                * inputNeuron.getDefect()
                                * network.getAxonFunction().derivative(inputNeuron.getActualOutputFromCore());
                        weight.getFactors().set(0, factor);
                    }
                }
            }

            for(NeuralLayerInterface layer : network.getLayers()) {
                for(NeuronInterface neuron : layer.getNeurons()) {
                    for(int i = 0; i < neuron.getWeights().size(); i++) {
                        WeightFunction weight = neuron.getWeights().get(i);
                        if(neuron.getWeightFunctionRecipe() == WeightFunctionRecipe.DEFAULT_LINEAR) {
                            Double factor = weight.getFactors().get(0);
                            factor = factor + deepLearningConfigurations.learningRate
                                    * neuron.getActualInputs().get(i)
                                    * neuron.getDefect()
                                    * network.getAxonFunction().derivative(neuron.getActualOutputFromCore());
                            weight.getFactors().set(0, factor);
                        }
                    }
                }
            }
        }
        return network;
    }

    @Override
    public String getSenderName() {
        return "DEEP-LRN";
    }
}
