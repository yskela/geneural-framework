package aleksy.geneuralframework.teaching.logic;

import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.common.util.GeneuralLogger;
import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmConfigurationsException;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmException;
import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import aleksy.geneuralframework.teachingdata.model.TeachingDataPackage;

import java.util.*;

/**
 * Genetic algorithm class
 */
public class GeneticAlgorithm implements Loggable {
    private Double bestAvgDistance;

    /**
     * Start method for genetic algorithm.
     * @param teachingData with packages to analyze distances and create rate value for network
     * @param neuralNetwork basic neural network
     * @param configurations with genetic algorithm parameters
     * @return list of neural networks after evolving process
     * @throws GeneticAlgorithmException when there is problem with genetic algorithm configurations
     */
    public List<NeuralNetworkInterface> start(TeachingData teachingData, NeuralNetworkInterface neuralNetwork,
                             GeneticAlgorithmConfigurations configurations) throws GeneticAlgorithmException {
        GeneuralLogger.log("Genetic algorithm starting.", getSenderName());
        GeneuralLogger.log("Checking the configurations correctness.", getSenderName());
        if(configurations.mutationChance > 1 || configurations.mutationChance < 0)
            throw new GeneticAlgorithmConfigurationsException("Mutation chance should be from interval 0.0 - 1.0");
        if(configurations.percentageAmountOfHybridizedNetworks > 1 || configurations.percentageAmountOfHybridizedNetworks < 0)
            throw new GeneticAlgorithmConfigurationsException("Percentage amount of  hybridized networks be from interval 0.0 - 1.0");
        if(configurations.generations < 1)
            throw new GeneticAlgorithmConfigurationsException("Generations number should be bigger than 0");
        if(configurations.expectedAmountOfNetworksOnOutput > configurations.amountOfNetworks)
            throw new GeneticAlgorithmConfigurationsException("Amount of networks on output should be less than/equal amount of networks on input");
        GeneuralLogger.log("Configurations are correct, starting an algorithm with:", getSenderName());
        GeneuralLogger.log("    :mutation chance: " + configurations.mutationChance, getSenderName());
        GeneuralLogger.log("    :percentage amount of good networks to hybridize: "
                + configurations.percentageAmountOfHybridizedNetworks, getSenderName());
        GeneuralLogger.log("    :amount of generations: " + configurations.generations, getSenderName());
        bestAvgDistance = Double.MAX_VALUE;
        List<NeuralNetworkInterface> population = new ArrayList<>();
        Random rand = new Random();
        for(int i = 0; i < configurations.amountOfNetworks; i++) {
            NeuralNetworkInterface n = neuralNetwork.copy();
            n.randomizeWeights();
            population.add(n);
        }
        GeneuralLogger.log("First generation of " + population.size() + " networks created.", getSenderName());

        Queue<NeuralNetworkInterface> sortedNetworks = rateGeneration(population, teachingData);
        int generation = 0;
        while(generation++ <= configurations.generations) {
            List<NeuralNetworkInterface> goodNetworks = new ArrayList<>();
            int amountOfGoodSolutions = (int)(configurations.percentageAmountOfHybridizedNetworks * configurations.amountOfNetworks);
            for(int i = 0; i < amountOfGoodSolutions; i++) {
                goodNetworks.add(sortedNetworks.poll());
            }
            population = new ArrayList<>();
            for(int i = 0; i < configurations.amountOfNetworks; i++) {
                population.add(goodNetworks.get(rand.nextInt(amountOfGoodSolutions))
                        .hybridize(goodNetworks.get(rand.nextInt(amountOfGoodSolutions)), configurations.mutationChance));
            }
            sortedNetworks = rateGeneration(population, teachingData);
            if(generation % 10 == 0)
                GeneuralLogger.log("Generation " + generation
                        + "/" + configurations.generations + ". The smallest average distance: "
                        + bestAvgDistance, getSenderName());
        }
        List<NeuralNetworkInterface> networksToReturn = new ArrayList<>();
        GeneuralLogger.log("Finishing. Returning " + configurations.expectedAmountOfNetworksOnOutput
                + " neural networks.", getSenderName());
        for(int i = 0; i < configurations.expectedAmountOfNetworksOnOutput; i++) {
            networksToReturn.add(sortedNetworks.poll());
        }
        return networksToReturn;
    }

    private NeuralNetworkInterface hybridize(NeuralNetworkInterface parent1, NeuralNetworkInterface parent2, double mutationChance) {
        return parent1.hybridize(parent2, mutationChance);
    }

    private Queue<NeuralNetworkInterface> rateGeneration(List<NeuralNetworkInterface> population, TeachingData teachingData) {
        Queue<NeuralNetworkInterface> sortedNetworks = new PriorityQueue<>();
        for(int i = 0; i < population.size(); i++) {
            NeuralNetworkInterface net = population.get(i);
            Double avgDistance = 0.0;
            for(TeachingDataPackage tp : teachingData.getDataPackages()) {
                try {
                    tp.actualOuts = net.f(tp.ins);
                } catch (NeuralNetworkException e) {
                    e.printStackTrace();
                }
                Double distance = 0.0;
                for(int j = 0; j < tp.actualOuts.size(); j++)
                    distance += Math.pow(tp.expectedOuts.get(j) - tp.actualOuts.get(j), 2);
                distance = Math.sqrt(distance);
                avgDistance += distance;
            }
            avgDistance /= teachingData.getDataPackages().size();
            net.setRate(avgDistance);
            if(avgDistance < bestAvgDistance)
                bestAvgDistance = avgDistance;
            sortedNetworks.add(net);
        }
        return sortedNetworks;
    }

    @Override
    public String getSenderName() {
        return "GENETIC";
    }
}
