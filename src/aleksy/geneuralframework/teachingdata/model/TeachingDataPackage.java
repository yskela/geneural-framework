package aleksy.geneuralframework.teachingdata.model;

import java.util.List;

/**
 * Single package of neural network teaching data
 */
public class TeachingDataPackage {
    /**
     * Input vector for neural network
     */
    public List<Double> ins;
    /**
     * Expected output vector from neural network
     */
    public List<Double> expectedOuts;
    /**
     * Actual output vector returned from network
     */
    public List<Double> actualOuts;
}
