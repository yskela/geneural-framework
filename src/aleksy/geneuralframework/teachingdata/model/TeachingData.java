package aleksy.geneuralframework.teachingdata.model;

import java.util.List;

/**
 * Teaching data with list of packages to teach neural network
 */
public class TeachingData {
    /**
     * Packages of data with input and expected output vectors
     */
    private List<TeachingDataPackage> dataPackages;

    /**
     * Setter for data packages
     * @param dataPackages to set
     */
    public void setDataPackages(List<TeachingDataPackage> dataPackages) {
        this.dataPackages = dataPackages;
    }

    /**
     * Getter for data packages
     * @return data packages
     */
    public List<TeachingDataPackage> getDataPackages() {
        return dataPackages;
    }
}
