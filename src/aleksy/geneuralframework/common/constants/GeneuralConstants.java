package aleksy.geneuralframework.common.constants;

/**
 * Constants values from Geneural Framework
 */
public class GeneuralConstants {
    /**
     * Geneural Framework Version
     */
    public static final String VERSION = "1.0.0";
    /**
     * Framework name
     */
    public static final String NAME = "Geneural Framework";
    /**
     * Framework author
     */
    public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
    /**
     * Bias default value for neurons used in framework
     */
    public static final Double BIAS = 1.0;
}
