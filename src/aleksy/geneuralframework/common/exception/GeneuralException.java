package aleksy.geneuralframework.common.exception;

/**
 * General exception thrown from GeneuralFramework
 */
public class GeneuralException extends Exception {
    public GeneuralException() {
        super();
    }

    public GeneuralException(String message) {
        super(message);
    }
}
