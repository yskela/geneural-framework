package aleksy.geneuralframework.neuralnetstructure.enumerate;

/**
 * Neural network types
 */
public enum NeuralNetworkType {
    /**
     * Single- or multilayer feedforward type
     */
    FEEDFORWARD,
    /**
     * Self-organising map, Kohonen's neural network
     */
    SELF_ORGANISING_MAP,
    /**
     * Reccurent neural network, Hopfield's neural network
     */
    RECCURENT
}
