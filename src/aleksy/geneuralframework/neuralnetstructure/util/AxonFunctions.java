package aleksy.geneuralframework.neuralnetstructure.util;

import aleksy.geneuralframework.neuralnetstructure.enumerate.AxonFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.model.function.AbstractAxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;

/**
 * Util class for axon functions
 */
public class AxonFunctions {
    private static class TANH extends AbstractAxonFunction {
        @Override
        public Double f(Double x) {
            return Math.tanh(x);
        }

        @Override
        public Double derivative(Double x) {
            return 1.0 / (Math.cosh(x) * Math.cosh(x));
        }
    }

    private static class LINEAR extends AbstractAxonFunction {
        @Override
        public Double f(Double x) {
            return x;
        }

        @Override
        public Double derivative(Double x) {
            return 1.0;
        }
    }

    private static class THRESHOLD extends AbstractAxonFunction {
        @Override
        public Double f(Double x) {
            if(x > 0.0)
                return 1.0;
            else
                return 0.0;
        }

        @Override
        public Double derivative(Double x) {
            return 0.0;
        }
    }

    private static class SINH extends AbstractAxonFunction {
        @Override
        public Double f(Double x) {
            return Math.sinh(x);
        }

        @Override
        public Double derivative(Double x) {
            return Math.cosh(x);
        }
    }

    /**
     * Method creates the new instance of axon function to set to neural network
     * @param axonFunctionRecipe says which kind of axon function method creates
     * @return new instance of axon function
     */
    public static AxonFunction newInstance(AxonFunctionRecipe axonFunctionRecipe) {
        AxonFunction axon = null;
        if(axonFunctionRecipe == AxonFunctionRecipe.TANH)
            axon = new TANH();
        if(axonFunctionRecipe == AxonFunctionRecipe.LINEAR)
            axon = new LINEAR();
        if(axonFunctionRecipe == AxonFunctionRecipe.THRESHOLD)
            axon = new THRESHOLD();
        if(axonFunctionRecipe == AxonFunctionRecipe.SINH)
            axon = new SINH();
        if(axon != null)
            axon.setAxonFunctionRecipe(axonFunctionRecipe);
        return axon;
    }
}
