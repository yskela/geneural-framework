package aleksy.geneuralframework.neuralnetstructure.config;

import aleksy.geneuralframework.neuralnetstructure.enumerate.AxonFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.enumerate.CoreFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.enumerate.NeuralNetworkType;
import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;

/**
 * Structure configurations for {@link aleksy.geneuralframework.neuralnetstructure.builder.GeneuralNetworkBuilder}
 */
public class StructureConfigurations {
    /**
     * Recipe for weight function class
     */
    public WeightFunctionRecipe weightFunctionRecipe;
    /**
     * Recipe for core function class
     */
    public CoreFunctionRecipe coreFunctionRecipe;
    /**
     * Recipe for axon function class
     */
    public AxonFunctionRecipe axonFunctionRecipe;
    /**
     * Type of neural network (for version 0.2.0 only FEEDFORWARD supported)
     */
    public NeuralNetworkType neuralNetworkType;
    /**
     * Bias presence in neural network. If true, all neurons have additional weight always with BIAS value from
     * {@link aleksy.geneuralframework.common.constants.GeneuralConstants}
     */
    public boolean biasPresence;
    /**
     * Input data normalization in first step of neural network work
     */
    public boolean dataNormalization;
}
