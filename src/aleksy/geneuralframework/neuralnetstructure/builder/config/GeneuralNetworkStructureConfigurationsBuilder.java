package aleksy.geneuralframework.neuralnetstructure.builder.config;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.common.util.GeneuralLogger;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.enumerate.AxonFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.enumerate.CoreFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.enumerate.NeuralNetworkType;
import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.util.AxonFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.CoreFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.WeightFunctions;

/**
 * Neural network structure configurations builder
 */
public class GeneuralNetworkStructureConfigurationsBuilder implements Loggable {
    private StructureConfigurations sc;
    private GeneuralFacadeInterface geneural;

    /**
     * Constructor
     */
    public GeneuralNetworkStructureConfigurationsBuilder(GeneuralFacadeInterface geneural) {
        sc = new StructureConfigurations();
        this.geneural = geneural;
    }

    /**
     * Setter for weight function for neural network structure configuration
     * @param weightFunctionRecipe to define
     * @return builder
     */
    public GeneuralNetworkStructureConfigurationsBuilder setWeightFunction(WeightFunctionRecipe weightFunctionRecipe) {
        sc.weightFunctionRecipe = weightFunctionRecipe;
        return this;
    }

    /**
     * Setter for core function for neural network structure configuration
     * @param coreFunctionRecipe to define
     * @return builder
     */
    public GeneuralNetworkStructureConfigurationsBuilder setCoreFunction(CoreFunctionRecipe coreFunctionRecipe) {
        sc.coreFunctionRecipe = coreFunctionRecipe;
        return this;
    }

    /**
     * Setter for axon function for neural network structure configuration
     * @param axonFunctionRecipe to define
     * @return builder
     */
    public GeneuralNetworkStructureConfigurationsBuilder setAxonFunction(AxonFunctionRecipe axonFunctionRecipe) {
        sc.axonFunctionRecipe = axonFunctionRecipe;
        return this;
    }

    /**
     * Setter for neural network type
     * @param neuralNetworkType to define
     * @return builder
     */
    public GeneuralNetworkStructureConfigurationsBuilder setNeuralNetworkType(NeuralNetworkType neuralNetworkType) {
        sc.neuralNetworkType = neuralNetworkType;
        return this;
    }

    /**
     * Setter for bias presence
     * @param biasPresence to set
     * @return builder
     */
    public GeneuralNetworkStructureConfigurationsBuilder setBiasPresence(boolean biasPresence) {
        sc.biasPresence = biasPresence;
        return this;
    }

    /**
     * Defines default configuration of neural network structure. Weight function
     * is set to DEFAULT_LINEAR from {@link WeightFunctions}, core function
     * - to DEFAULT_ADDER from {@link CoreFunctions}, axon function - to TANH from {@link AxonFunctions}.
     * The type of network is FEEDFORWARD
     * @return builder
     */
    public GeneuralNetworkStructureConfigurationsBuilder defaultConfigurations() {
        sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
        sc.axonFunctionRecipe = AxonFunctionRecipe.TANH;
        sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
        sc.biasPresence = true;
        sc.dataNormalization = true;
        geneural.log("Default structure configurations built.", getSenderName());
        return this;
    }

    /**
     * Setter for data normalization. Use it when you would to scale neural network
     * input data to average of input data values.
     * @param dataNormalization to set
     * @return builder
     */
    public GeneuralNetworkStructureConfigurationsBuilder setDataNormalization(Boolean dataNormalization) {
        sc.dataNormalization = dataNormalization;
        return this;
    }

    /**
     * Builds a configurations object
     * @return structure configurations with parameters set in builder
     */
    public StructureConfigurations create() {
        return sc;
    }

    @Override
    public String getSenderName() {
        return "STRUCT-CONFIG-BLD";
    }
}
