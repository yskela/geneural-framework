package aleksy.geneuralframework.neuralnetstructure.builder;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.common.util.GeneuralLogger;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;
import aleksy.geneuralframework.neuralnetstructure.util.WeightFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder of neural network with defined input layer
 */
public class NetworkWithInputLayerBuilder implements Loggable {

    private StructureConfigurations configurations;
    private NeuralNetworkInterface neuralNetwork;
    private CoreFunction core;
    private AxonFunction axon;
    private GeneuralFacadeInterface geneural;

    /**
     * Constructor
     * @param configurations for neural network
     * @param neuralNetwork to build
     * @param core instance of core function to set in every neuron of network
     * @param axon instance of axon function to set in every neuron of network
     */
    public NetworkWithInputLayerBuilder(StructureConfigurations configurations, NeuralNetworkInterface neuralNetwork,
                                        CoreFunction core, AxonFunction axon, GeneuralFacadeInterface geneural) {
        this.configurations = configurations;
        this.neuralNetwork = neuralNetwork;
        this.core = core;
        this.axon = axon;
        this.geneural = geneural;
    }

    /**
     * Adds new (hidden) layer with default description to network
     * @param amountOfNeurons in new layer
     * @return builder
     */
    public NetworkWithInputLayerBuilder addLayer(int amountOfNeurons) {
        int amountOfInputs = neuralNetwork.getLayers()
                .get(neuralNetwork.numberOfLayers() - 1).size();
        NeuralLayerInterface layer = prepareLayer(amountOfNeurons, amountOfInputs);
        layer.setDescription("layer created in builder");
        neuralNetwork.getLayers().add(layer);
        return this;
    }

    /**
     * Adds new (hidden) layer with custom description to network
     * @param amountOfNeurons in new layer
     * @param description of layer
     * @return builder
     */
    public NetworkWithInputLayerBuilder addLayer(int amountOfNeurons, String description) {
        int amountOfInputs = neuralNetwork.getLayers()
                .get(neuralNetwork.numberOfLayers() - 1).size();
        NeuralLayerInterface layer = prepareLayer(amountOfNeurons, amountOfInputs);
        layer.setDescription(description);
        neuralNetwork.getLayers().add(layer);
        return this;
    }

    /**
     * Ends the neural network building.
     * @param randomizeWeights decision parameter. If it's set to true, new neural network has
     *                         randomized weight factors.
     * @return endpoint builder
     */
    public EndpointBuilder endBuildingNetwork(boolean randomizeWeights) {
        geneural.log("Neural network with " + neuralNetwork.getLayers().size() + " layers created.", getSenderName());
        geneural.log("Amount of neurons in network: " + neuralNetwork.size() + ".", getSenderName());
        if(randomizeWeights) {
            neuralNetwork.randomizeWeights();
            geneural.log("Weights of neurons of this network was randomized.", getSenderName());
        }
        return new EndpointBuilder(neuralNetwork);
    }

    private NeuralLayerInterface prepareLayer(int amountOfNeurons, int amountOfInputs) {
        NeuralLayerInterface layer = new GeneuralLayer();
        layer.setInputLayer(false);
        List<NeuronInterface> neurons = new ArrayList<>();
        for(int i = 0; i < amountOfNeurons; i++) {
            List<WeightFunction> weights = new ArrayList<>();
            for(int j = 0; j < amountOfInputs; j++) {
                weights.add(WeightFunctions.newInstance(configurations.weightFunctionRecipe, false));
            }
            NeuronInterface neuron = new GeneuralNeuron(weights, core, axon, neuralNetwork.isBiasPresence());
            neurons.add(neuron);
        }
        layer.setNeurons(neurons);
        return layer;
    }

    @Override
    public String getSenderName() {
        return "NETWORK-BLD";
    }
}
