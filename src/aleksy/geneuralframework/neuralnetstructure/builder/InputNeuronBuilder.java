package aleksy.geneuralframework.neuralnetstructure.builder;

/**
 * Input neuron builder
 */
public class InputNeuronBuilder {
    /**
     * Makes nothing
     * @return builder
     */
    @Deprecated
    public InputNeuronBuilder addInput() {
        return this;
    }
}
