package aleksy.geneuralframework.neuralnetstructure.exception.abstr;

import aleksy.geneuralframework.common.exception.GeneuralException;
import aleksy.geneuralframework.common.util.GeneuralLogger;

/**
 * Neural network exception
 */
public abstract class NeuralNetworkException extends GeneuralException {
    public NeuralNetworkException(String message) {
        super("neural network -> " + message);
    }
}
