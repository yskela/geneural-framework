package aleksy.geneuralframework.neuralnetstructure.exception.abstr;

/**
 * Neural layer exception
 */
public class NeuralLayerException extends NeuralNetworkException {
    public NeuralLayerException(String message) {
        super("layer -> " + message);
    }
}
