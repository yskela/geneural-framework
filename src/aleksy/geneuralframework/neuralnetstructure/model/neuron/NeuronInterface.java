package aleksy.geneuralframework.neuralnetstructure.model.neuron;

import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;

import java.util.List;

/**
 * Neuron interface
 */
public interface NeuronInterface {
    /**
     * Function calculates output of neuron
     * @param xs vector of inputs
     * @return calculated output in specific type
     * @throws IncorrectNumberOfInputsInNeuronException when number of inputs is not equals to number of weights functions
     */
    Double f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException;

    /**
     * Setter for neuron description. Description is useful for input neurons when network is small
     * and has defined inputs
     * @param description description to set
     */
    void setDescription(String description);

    /**
     * Getter for neuron description
     * @return neuron description
     */
    String getDescription();

    /**
     * Size of neuron
     * @return number of inputs
     */
    int size();

    /**
     * Randomizes weights functions
     */
    void randomizeWeights();

    /**
     * Clones weights from neuron
     * @return new list cloned weights
     */
    List<WeightFunction> cloneWeights();

    /**
     * Getter for instance of core function
     * @return core function instance
     */
    CoreFunction getCoreFunction();

    /**
     * Getter for instance of axon function
     * @return axon function instance
     */
    AxonFunction getAxonFunction();

    /**
     * Hybridizes neuron with another neuron given in parameter
     * @param neuron to hybridize
     * @param mutationChance of single factor in weight in vector
     * @return new child of two neurons
     */
    NeuronInterface hybridize(NeuronInterface neuron, double mutationChance);

    /**
     * Getter for bias presence
     * @return bias presence
     */
    boolean isBiasPresence();

    /**
     * Setter for bias presence
     * @param biasPresence to set
     */
    void setBiasPresence(boolean biasPresence);

    /**
     * Adds one random weight to neuron when network has bias presence
     */
    void addRandomBiasWeight();

    /**
     * Setter for weight function recipe
     * @param weightFunctionRecipe to set
     */
    void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe);

    /**
     * Getter for weight function recipe
     * @return weight function recipe
     */
    WeightFunctionRecipe getWeightFunctionRecipe();

    /**
     * Setter to defect of neuron (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @param defect to set
     */
    void setDefect(Double defect);

    /**
     * Getter for defect of neuron (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @return defect of neuron
     */
    Double getDefect();

    /**
     * Getter for weights list of neuron
     * @return weights list
     */
    List<WeightFunction> getWeights();

    /**
     * Setter for actual output from core (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @param actualOutputFromCore to set
     */
    void setActualOutputFromCore(Double actualOutputFromCore);

    /**
     * Getter for actual output from core (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @return actual output from core
     */
    Double getActualOutputFromCore();

    /**
     * Setter for actual input vector of neuron (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @param actualInputs to set
     */
    void setActualInputs(List<Double> actualInputs);

    /**
     * Getter for actual input vector of neuron (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @return input vector
     */
    List<Double> getActualInputs();

    /**
     * Setter for actual axon output (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @param axonOutput to set
     */
    void setActualAxonOutput(Double axonOutput);

    /**
     * Getter for actual axon output (See also {@link aleksy.geneuralframework.teaching.logic.DeepLearning})
     * @return actual axon output
     */
    Double getActualAxonOutput();
}
