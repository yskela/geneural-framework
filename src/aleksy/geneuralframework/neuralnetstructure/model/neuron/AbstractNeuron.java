package aleksy.geneuralframework.neuralnetstructure.model.neuron;

import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;
import aleksy.geneuralframework.neuralnetstructure.util.WeightFunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Abstract neuron
 */
public abstract class AbstractNeuron implements NeuronInterface {

    protected List<WeightFunction> weights;
    protected CoreFunction core;
    protected AxonFunction axon;
    protected String description;
    protected boolean biasPresence;
    protected WeightFunctionRecipe weightFunctionRecipe;
    protected Double defect;
    protected Double actualOutputFromCore;
    protected List<Double> actualInputs;
    protected Double actualAxonOutput;

    public AbstractNeuron(List<WeightFunction> weights, CoreFunction core, AxonFunction axon, boolean biasPresence) {
        this.weights = weights;
        this.core = core;
        this.axon = axon;
        this.weightFunctionRecipe = weights.get(0).getWeightFunctionRecipe();
        this.biasPresence = biasPresence;
        description = "default neuron description";
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Double f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException {
        if(xs.size() != weights.size())
            throw new IncorrectNumberOfInputsInNeuronException("neuron description: " + description);
        List<Double> weightsOut = new ArrayList<>();
        for(int i = 0; i < xs.size(); i++) {
            weightsOut.add(weights.get(i).f(xs.get(i)));
        }
        actualInputs = new ArrayList<>(xs);
        actualOutputFromCore = core.f(weightsOut);
        actualAxonOutput = axon.f(actualOutputFromCore);
        return actualAxonOutput;
    }

    @Override
    public int size() {
        return weights.size();
    }

    @Override
    public String toString() {
        return "neuron: " + weights.toString();
    }

    @Override
    public void randomizeWeights() {
        for(WeightFunction w : weights) {
            for(int i = 0; i < w.getFactors().size(); i++) {
                w.getFactors().set(i, 1 - 2 * new Random().nextDouble());
            }
        }
    }

    @Override
    public List<WeightFunction> cloneWeights() {
        List<WeightFunction> newWeights = new ArrayList<>();
        for(WeightFunction w : weights) {
            WeightFunction newWeight = WeightFunctions.newInstance(w.getWeightFunctionRecipe(), false);
            newWeights.add(newWeight);
            newWeight.setFactors(new ArrayList<>(w.getFactors()));
        }
        return newWeights;
    }

    @Override
    public CoreFunction getCoreFunction() {
        return core;
    }

    @Override
    public AxonFunction getAxonFunction() {
        return axon;
    }

    @Override
    public boolean isBiasPresence() {
        return biasPresence;
    }

    @Override
    public void setBiasPresence(boolean biasPresence) {
        this.biasPresence = biasPresence;
    }

    @Override
    public NeuronInterface hybridize(NeuronInterface neuron, double mutationChance) {
        List<WeightFunction> weights1 = cloneWeights();
        List<WeightFunction> weights2 = neuron.cloneWeights();
        List<WeightFunction> childWeights = new ArrayList<>();
            for(int i = 0; i < weights1.size(); i++) {
                List<Double> factors1 = weights1.get(i).getFactors();
                List<Double> factors2 = weights2.get(i).getFactors();
                List<Double> childFactors = new ArrayList<>();
                for(int j = 0; j < factors1.size(); j++) {
                    Double factorToAdd;
                    if(new Random().nextBoolean())
                        factorToAdd = factors1.get(j);
                    else
                        factorToAdd = factors2.get(j);
                    //factorToAdd = (factors1.get(j) + factors2.get(j)) / 2;
                    if(new Random().nextDouble() < mutationChance)
                        factorToAdd = new Random().nextDouble();
                    childFactors.add(factorToAdd);
                }
                WeightFunction w = WeightFunctions.newInstance(weights1.get(i).getWeightFunctionRecipe(), false);
                assert w != null;
                w.setFactors(childFactors);
                childWeights.add(w);
            }
            NeuronInterface childNeuron = new GeneuralNeuron(childWeights, core, axon, biasPresence);
            childNeuron.setDescription(description);
        return childNeuron;
    }

    @Override
    public void addRandomBiasWeight() {
        weights.add(WeightFunctions.newInstance(weights.get(0).getWeightFunctionRecipe(), true));
    }

    @Override
    public void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe) {
        this.weightFunctionRecipe = weightFunctionRecipe;
    }

    @Override
    public WeightFunctionRecipe getWeightFunctionRecipe() {
        return weightFunctionRecipe;
    }

    @Override
    public void setDefect(Double defect) {
        this.defect = defect;
    }

    @Override
    public Double getDefect() {
        return defect;
    }

    @Override
    public List<WeightFunction> getWeights() {
        return weights;
    }

    @Override
    public void setActualOutputFromCore(Double actualOutputFromCore) {
        this.actualOutputFromCore = actualOutputFromCore;
    }

    @Override
    public Double getActualOutputFromCore() {
        return actualOutputFromCore;
    }

    @Override
    public void setActualInputs(List<Double> actualInputs) {
        this.actualInputs = actualInputs;
    }

    @Override
    public List<Double> getActualInputs() {
        return actualInputs;
    }

    @Override
    public void setActualAxonOutput(Double actualAxonOutput) {
        this.actualAxonOutput = actualAxonOutput;
    }

    @Override
    public Double getActualAxonOutput() {
        return actualAxonOutput;
    }
}
