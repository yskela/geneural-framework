package aleksy.geneuralframework.neuralnetstructure.model.neuron.impl;

import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.AbstractNeuron;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;

import java.util.List;

/**
 * Basic implementation of neuron
 */
public class GeneuralNeuron extends AbstractNeuron implements NeuronInterface {
    public GeneuralNeuron(List<WeightFunction> weights, CoreFunction core, AxonFunction axon, boolean biasPresence) {
        super(weights, core, axon, biasPresence);
    }
}
