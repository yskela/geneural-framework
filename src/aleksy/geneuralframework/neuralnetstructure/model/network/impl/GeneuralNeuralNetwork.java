package aleksy.geneuralframework.neuralnetstructure.model.network.impl;

import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.network.AbstractNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic implementation of neural network
 */
public class GeneuralNeuralNetwork extends AbstractNeuralNetwork implements NeuralNetworkInterface {
}
