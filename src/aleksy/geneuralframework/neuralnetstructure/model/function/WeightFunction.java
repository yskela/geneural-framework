package aleksy.geneuralframework.neuralnetstructure.model.function;


import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;

import java.util.List;

/**
 * Weight function interface
 */
public interface WeightFunction extends Function {
    /**
     * Function calculates one internal output for core function of neuron from one external input
     * @param x external input
     * @return calculated internal output
     */
    Double f(Double x);


    /**
     * Setter for factors
     * @param factors list of factors to set
     */
    void setFactors(List<Double> factors);

    /**
     * Getter for factors
     * @return list of factors of weight function
     */
    List<Double> getFactors();

    /**
     * Definition of factors initialization after creating an object from constructor
     */
    void initFactors();

    /**
     * Setter for weight function recipe
     * @param weightFunctionRecipe to set
     */
    void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe);

    /**
     *Getter for weight function recipe
     * @return weight function recipe
     */
    WeightFunctionRecipe getWeightFunctionRecipe();
}
